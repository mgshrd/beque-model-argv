Require Import sublist.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

(** * A collection of actions and properties. E.g. file operations, mutex operations, etc. *)
Require Import Beque.Frame.util.ExistentialAction.

Require Import (*PACKAGE.*)Domain.

Require Import ArgvActions.

(** * Argv summary *)
Require Import Beque.Util.DelayExtractionDefinitionToken.

Require Import (*PACKAGE.*)Argv.

Module ArgvImpl
       (w : World)
       (ior : IORaw w)
<: Argv w ior
.
Module aam := ArgvActions.

Module io := IOAux w ior.
Module ltl := LTL w.

(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
Definition action :
  Set.
Proof.
  exact aam.argv_action.
Defined.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  exact aam.eq_argv_action_dec.
Defined.

(** Specify the type of the result when [a] is executed *)
Definition action_res :
  forall (a : action),
    Set.
Proof.
  exact aam.argv_action_res.
Defined.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  exact aam.eq_argv_action_res_dec.
Defined.

(** The properties this domain defines. *)
Inductive argv_prop :=.
Definition prop :
  Set.
Proof.
  exact argv_prop.
Defined.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof.
  decide equality.
Defined.


(*************** [Domain] action marker *******************)

(** Marker expressing that the segment of messages [s] was generated during performing action [a] which produced the value [res]. *)
Axiom segment_of_action_with_res:
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.
(* note the lack of [segment_of_action_with_res_dec]: it is not required to be able to retroactively identify segments after they are generated. *)
(* TODO investigate contains_segment_of_action_with_res to support parallel executions interleaved traces instead of forcing all domains to be serial Parameter contains_segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg)
    (trace : list w.msg),
    Prop. *)




(*************** [Domain] action and property interactions *******************)

(** Specify property propagations guaranteed by this domain.

If a trace [t] has property [p], executing action [a] which results in [res] and adds a segment of messages [s], does not interfere with property [p], that is trace [s ++ t] still has property [p].
*)
Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros.
  exact (match p, a, res with
         end).
Defined.
Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, p; firstorder.
Defined.

(** Specify backwards property propagations guaranteed by this domain.

If a trace [s++t] has property [p], where [s] is the segment of messages generated while executing action [a] which resulted in [res], then [p] already did hold on [t] as well.
*)
Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop.
Proof.
  intros.
  exact (match p, a, res with
         end).
Defined.
Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p}.
Proof.
  intros.
  destruct a, p; firstorder.
Defined.



(*************** [Domain] denotations *******************)

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

(** if a [prop] is closely linked to an [action] this helper creates trace property, where the action segment of [a] starts the valid range, and it propagates until [p] propagates *)
Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

(** Translate property ids into trace properties. *)
Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop.
Proof.
  intros ?.
  exact (match p with
         end).
Defined.

(** Translate an action id into a IO action *)
Module ea := ExistentialAction w ior.
Definition plain_action :=
  ea.plain_action
    action_res
    segment_of_action_with_res
    (fun _ => True) (* all actions callable all the time *).

Axiom get_args:
  plain_action
      (aam.argv_action__get_args).

Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type }.
Proof.
  intros.
  destruct a.
  - refine (existT _ (_, _) (get_args, _)); [ solve [ auto ] ].
Defined.




(********************** [Domain] consistency *************************)
(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)
Lemma propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).
Proof.
  destruct p.
Qed.

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)
Lemma back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.
Proof.
  destruct p.
Qed.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

Module extractHaskell (token : DelayExtractionDefinitionToken).
Extract Constant get_args => "Prelude.fmap (Prelude.fmap Prelude.read) System.Environment.getArgs".
End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken).
Extract Constant get_args => "fun () -> List.tl (Array.to_list (Array.map string2coqstring Sys.argv))".
End extractOcaml.

End ArgvImpl.
