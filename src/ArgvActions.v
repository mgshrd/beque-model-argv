Require Import List.
Require Import String.

Module ArgvActions
.

Inductive argv_action :=
| argv_action__get_args : argv_action.

Definition eq_argv_action_dec :
  forall a1 a2 : argv_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  decide equality.
Defined.

Definition argv_action_res
           (a : argv_action) :
  Set :=
  match a with
  | argv_action__get_args => list string
  end.

Definition eq_argv_action_res_dec :
  forall (a : argv_action) (res1 res2 : argv_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  assert (se := string_dec).
  assert (le := list_eq_dec string_dec).
  destruct a; simpl in *; decide equality.
Defined.

End ArgvActions.
