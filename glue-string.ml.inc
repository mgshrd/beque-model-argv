let string2coqstring s =
  let r = ref EmptyString in
  for i = 1 to String.length s do
    r := String (String.get s (String.length s - i), !r)
  done;
  !r
let coqstring2string cs =
  let r = Buffer.create 16 in
  let rec loop csx =
    match csx with
    | EmptyString -> ()
    | String (a, s1) -> Buffer.add_char r a; loop s1
  in loop cs; Buffer.contents r
