Require Import Beque.Util.DelayExtractionDefinitionToken.
Require Beque.Util.common_extract.
Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.

Module Exts.
Module extractHaskell (token : DelayExtractionDefinitionToken).
Module comext := common_extract.commonExtractHaskell token.
Module ioext := AxiomaticIOInAUnitWorld.extractHaskell token.
End extractHaskell.
Module extractOcaml (token : DelayExtractionDefinitionToken).
Module comext := common_extract.commonExtractOcaml token.
Module ioext := AxiomaticIOInAUnitWorld.extractOcaml token.
End extractOcaml.
End Exts.
