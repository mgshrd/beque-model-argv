Require Import List.
Require Import String.

Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.

Require Import ArgvImplUWA.
Require Import Exts.

Extraction Language Haskell.

Module exts := Exts.extractHaskell DelayExtractionDefinitionToken.token.
Module argvext := ArgvImplUWA.extractHaskell DelayExtractionDefinitionToken.token.

Module Dump.

Axiom dump : string -> AxiomaticIOInAUnitWorld.IO (fun _ => True) unit (fun _ _ _ => True).

Module extractHaskell (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant dump => "Prelude.putStrLn Prelude.. Prelude.show".
End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant dump => "fun s -> fun () -> print_endline (coqstring2string s)".
End extractOcaml.

End Dump.

Module dumpeext := Dump.extractHaskell DelayExtractionDefinitionToken.token.

Module ior := IOAux UnitWorld AxiomaticIOInAUnitWorld.
Import ior.

Program Definition main :=
  a <== ArgvImplUWA.get_args;;
    /| Dump.dump (match a with nil => "<no args>" | h :: _ => h end).

Extraction "test" main.
